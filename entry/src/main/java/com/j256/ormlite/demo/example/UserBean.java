/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.j256.ormlite.demo.example;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Function description
 * UserBean
 *
 */
@DatabaseTable(tableName = "user")
public class UserBean {
    /**
     * COLUMNNAME_ID
     */
    public static final String COLUMNNAME_ID = "id";

    /**
     * COLUMNNAME_NAME
     */
    public static final String COLUMNNAME_NAME = "name";

    /**
     * COLUMNNAME_LAST_NAME
     */
    public static final String COLUMNNAME_LAST_NAME = "lastName";

    @DatabaseField(generatedId = true, columnName = COLUMNNAME_ID, useGetSet = true)
    private int id;
    @DatabaseField(columnName = COLUMNNAME_NAME, useGetSet = true, canBeNull = false)
    private String name;
    @DatabaseField(columnName = COLUMNNAME_LAST_NAME, useGetSet = true)
    private String lastName;

    /**
     * constructor
     */
    public UserBean() {
    }

    /**
     * UserBean Data
     *
     * @param lastName String
     * @param name String
     */
    public UserBean(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "UserBean{" + "id=" + id + ", name='" + name + '\'' + ", lastName=" + lastName + '}';
    }
}
