/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.j256.ormlite.demo.example;

import com.j256.ormlite.db.SqlException;
import com.j256.ormlite.utils.LogUtil;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.demo.Constants;
import com.j256.ormlite.stmt.*;

import ohos.app.Context;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

/**
 * Function description
 * UserBean
 */
public class UserDao {
    private static final String TAG = "UserDao";

    private Context context;

    // ORMLite提供的DAO类对象，第一个泛型是要操作的数据表映射成的实体类；第二个泛型是这个实体类中ID的数据类型
    private Dao<UserBean, Integer> dao;

    /**
     * UserDao Data
     *
     * @param icontext UserBean
     */
    public UserDao(Context icontext) {
        this.context = icontext;
        try {
            this.dao = Dbhelper.getInstance(icontext).getDao(UserBean.class);
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
    }

    /**
     * insert Data
     *
     * @param data UserBean
     */
    public void insert(UserBean data) {
        try {
            int val = dao.create(data);
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
    }

    /**
     * delete Data
     *
     * @param data UserBean
     * @throws SQLException Exception
     */
    public void delete(UserBean data) throws SQLException {
        dao.deleteById(1);
    }

    /**
     * deleteById Data
     *
     * @param var integer
     */
    public void deleteById(int var) {
        try {
            dao.deleteById(var);
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
    }

    /**
     * update Data
     *
     * @param data UserBean
     */
    public void update(UserBean data) {
        try {
            dao.update(data);
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
        LogUtil.error(TAG, "Update finished");
    }

    /**
     * query Data
     *
     * @return users list
     */
    public List<UserBean> query() {
        List<UserBean> users = null;
        try {
            users = dao.queryForAll();
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
        LogUtil.error(TAG, "Query finished");

        return users;
    }

    /**
     * queryById Data
     *
     * @param dataid integer
     * @return users list
     */
    public UserBean queryById(int dataid) {
        UserBean user = null;
        try {
            user = dao.queryForId(dataid);
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
        return user;
    }

    /**
     * queryBuilder Data
     */
    public void queryBuilder() {
        try {
            QueryBuilder<UserBean, Integer> builder = dao.queryBuilder();
            builder.orderBy(UserBean.COLUMNNAME_LAST_NAME, false).limit(Constants.MAGIC_30L);
            List<UserBean> list = dao.query(builder.prepare());
            Iterator<UserBean> itr = list.iterator();
            while (itr.hasNext()) {
                LogUtil.error(TAG, "entry" + itr.next());
            }
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
    }

    /**
     * deleteBuilder Data
     */
    public void deleteBuilder() {
        DeleteBuilder<UserBean, Integer> deleteBuilder =
                dao.deleteBuilder();
        try {
            deleteBuilder.where().eq("lastName", "qqqa");
            int deleteId = deleteBuilder.delete();
            LogUtil.error(TAG, "deleteBuilder" + deleteId);
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
    }

    /**
     * updateBuilder Data
     */
    public void updateBuilder() {
        UpdateBuilder<UserBean, Integer> updateBuilder = dao.updateBuilder();
        try {
            updateBuilder.where().eq("lastName", "a");
            updateBuilder.updateColumnValue("name" /* column */, "updated" /* value */);
            int updateId = updateBuilder.update();
            LogUtil.error(TAG, "updateBuilder" + updateId);
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
    }

    /**
     * queryMatch Data
     */
    public void queryMatch() {
        UserBean user = new UserBean("a", "b");
        try {
            List<UserBean> list = dao.queryForMatching(user);
            LogUtil.error(TAG, "list.get(0)" + list.get(0).getName());
            LogUtil.error(TAG, "list.get(1)" + list.get(0).getLastName());
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
    }

    /**
     * multipleQuery Data
     */
    public void multipleQuery() {
        try {
            QueryBuilder<UserBean, Integer> queryBuilder = dao.queryBuilder();
            Where<UserBean, Integer> where = queryBuilder.where();
            where.eq(UserBean.COLUMNNAME_NAME, "foo");
            where.and();
            where.eq(UserBean.COLUMNNAME_LAST_NAME, "test");
            PreparedQuery<UserBean> preparedQuery = queryBuilder.prepare();
            List<UserBean> list = dao.query(preparedQuery);
            Iterator<UserBean> itr = list.iterator();
            while (itr.hasNext()) {
                LogUtil.error(TAG, "entry :" + itr.next());
            }
        } catch (SqlException | SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }
    }
}