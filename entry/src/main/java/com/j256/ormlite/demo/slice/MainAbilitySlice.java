/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.j256.ormlite.demo.slice;

import com.j256.ormlite.demo.ResourceTable;
import com.j256.ormlite.demo.example.UserBean;
import com.j256.ormlite.demo.example.UserDao;
import com.j256.ormlite.demo.example.UserDaoAdapter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;

import java.util.List;

/**
 * PersonAbilitySlice
 *
 */
public class MainAbilitySlice extends AbilitySlice {
    private Button addPersonButton;
    private Button updatePersonButton;
    private Button deletePersonButton;

    private UserDao usrDao;

    private UserDaoAdapter userDaoAdapter;

    private List<UserBean> queryResult;
    private UserDaoAdapter.PersonClickListener personClickListener = new UserDaoAdapter.PersonClickListener() {
        @Override
        public void onPersonClick(int position) {
            UserBean person = userDaoAdapter.getPerson(position);
            int personId = person.getId();
            usrDao.deleteById(personId);
            update();
        }
    };

    private TextField mName;

    private TextField mLastName;

    private TextField mId;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        initView();
    }

    private void setUpViews() {
        mName.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                boolean isEnable = s.length() != 0;
                addPersonButton.setEnabled(isEnable);
            }
        });
        updatePersonButton.setClickedListener(component -> updatePerson());
        deletePersonButton.setClickedListener(component -> deletePerson());
    }

    private void deletePerson() {
        usrDao.deleteById(Integer.valueOf(mId.getText()));
        update();
    }

    private void update() {
        queryResult = usrDao.query();
        userDaoAdapter.setPeople(queryResult);
    }

    private void initView() {
        setUIContent(ResourceTable.Layout_main_ability_slice);
    }

    /**
     * updatePerson
     *
     */
    private void updatePerson() {
        UserBean user = usrDao.queryById(Integer.valueOf(mId.getText()));
        if (user != null) {
            user.setName(mName.getText());
            user.setLastName(mLastName.getText());
            usrDao.update(user);
            update();
        }
    }

    /**
     * addPerson
     *
     */
    private void addPerson() {
        UserBean userData = new UserBean(mName.getText(), mLastName.getText());
        usrDao.insert(userData);
        update();
    }

    /**
     * queryBuilderTest
     *
     */
    public void queryBuilderTest() {
        usrDao.queryBuilder();
    }

    /**
     * deleteBuilderTest
     *
     */
    public void deleteBuilderTest() {
        usrDao.deleteBuilder();
        update();
    }

    /**
     * updateBuilderTest
     *
     */
    public void updateBuilderTest() {
        usrDao.updateBuilder();
        update();
    }

    /**
     * queryforMatchingTest
     *
     */
    public void queryforMatchingTest() {
        usrDao.queryMatch();
    }

    /**
     * multipleQuery
     *
     */
    public void multipleQuery() {
        usrDao.multipleQuery();
    }

    /**
     * updateQuery
     *
     */
    public void updateQuery() {
    }
}
