/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.j256.ormlite.demo.example;

import com.j256.ormlite.db.SqlException;
import com.j256.ormlite.hmos.OrmLiteConfigUtil;
import com.j256.ormlite.utils.LogUtil;

import java.io.IOException;

/**
 * DatabaseConfigUtil
 *
 */
public class DatabaseConfigUtil extends OrmLiteConfigUtil {
    /**
     *
     * userBeans
     *
     */
    public static final Class<?>[] CLASSES = new Class[]{
            UserBean.class,
            UserDao.class
    };

    /**
     *
     * default constructor
     *
     * @param args argument
     * @throws SqlException exception
     * @throws IOException io exception
     */
    public static void main(String[] args) throws SqlException, IOException {
        try {
            writeConfigFile("ormlite_config.txt",CLASSES);
        } catch (java.sql.SQLException e) {
            LogUtil.debug("Exception",e.toString());
        }
    }
}
