/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.j256.ormlite.demo.slice;

import com.j256.ormlite.demo.ResourceTable;
import com.j256.ormlite.hmos.apptools.OrmLiteAbilitySlice;
import com.j256.ormlite.utils.LogUtil;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.demo.example.Dbhelper;
import com.j256.ormlite.demo.example.UserBean;

import ohos.aafwk.content.Intent;

import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

/**
 * RdbSlice
 *
 */
public class RdbSlice extends OrmLiteAbilitySlice<Dbhelper> implements Component.ClickedListener {
    private static final int MAX_NUM_TO_CREATE = 8;
    private static final String TAG = "Yjt";

    private ComponentContainer view;
    private TextField textFieldShow;
    private Text textShow;
    private Button btnAdd;
    private Button btnDel;
    private Button btnQuery;
    private Button btnUpdate;
    private Dao usrDao;
    private StringBuffer stBuffer = new StringBuffer();

    @Override
    public Class getRdbOpenHelperClass() {
        return Dbhelper.class;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        view = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_first_layout, null, false);
        setUIContent(view);
    }

    @Override
    public void onActive() {
        super.onActive();
        initializeViews();
    }

    /*
     * init call
     */
    private void initializeViews() {
        try {
            usrDao = getHelper().getDao(UserBean.class);
        } catch (SQLException e) {
            LogUtil.info(TAG, e.getMessage());
        }

        textShow = (Text) view.findComponentById(ResourceTable.Id_text_Show);
        textShow.setText("Initial Text Show");

        btnAdd = (Button) view.findComponentById(ResourceTable.Id_btn_Add);
        btnAdd.setClickedListener(this);

        btnDel = (Button) view.findComponentById(ResourceTable.Id_btn_Del);
        btnDel.setVisibility(Component.INVISIBLE);

        btnQuery = (Button) view.findComponentById(ResourceTable.Id_btn_Query);
        btnQuery.setClickedListener(this);

        btnUpdate = (Button) view.findComponentById(ResourceTable.Id_btn_Update);
        btnUpdate.setVisibility(Component.INVISIBLE);
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_Add:
                long millis = System.currentTimeMillis();
                UserBean userData1 = new UserBean("张三" + millis, "北京");
                UserBean userData2 = new UserBean("李四" + millis, "上海");
                UserBean userData3 = new UserBean("王五" + millis, "上海");
                try {
                    usrDao.create(new UserBean("张三" + millis, "北京"));

                    usrDao.create(userData1);
                    usrDao.create(userData2);
                    usrDao.create(userData3);
                } catch (SQLException e) {
                    LogUtil.info(TAG, e.getMessage());
                }
                showToast("Insert record finished!");
                break;

            case ResourceTable.Id_btn_Query:
                stBuffer.delete(0, stBuffer.length());
                List<UserBean> usersRet = null;
                stBuffer.append("\n\n");
                try {
                    usersRet = usrDao.queryForAll();

                    Iterator<UserBean> stItem = usersRet.iterator();
                    while (stItem.hasNext()) {
                        UserBean stUsrBean = stItem.next();
                        stBuffer.append(stUsrBean.toString() + "\n");
                    }
                    textShow.setText(stBuffer.toString());
                } catch (SQLException e) {
                    LogUtil.info(TAG, e.getMessage());
                }
                showToast("The query finished");
                break;
            default:
                LogUtil.error(TAG, "Unhandled click : " + component.getId() + " ");
        }
    }

    /*
     * Toast
     */
    private void showToast(String message) {
        new ToastDialog(this).setText(message).show();
    }
}
