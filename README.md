# ormlite-ohos

This package provides the ohos specific functionality. You will also need to download the [ormlite-core](https://github.com/j256/ormlite-core) package as well.
(ormlite-core - Core ORMLite functionality that provides a lite Java ORM in conjunction with ormlite-jdbc or ormlite-ohos)

# Usage Instructions
-----

To use the package you add the @DatabaseTable annotation to the top of each class and a @DatabaseField annotation to each of the fields in the class that are to be persisted to the database.Classes can also be configured with javax.persistence annotations (JPA), Java calls, or Spring wiring. For more details, see the on-line [documentation](https://ormlite.com/javadoc/ormlite-core/doc-files/ormlite.html). For example:

```
@DatabaseTable(tableName = "accounts")
public class Account {
    @DatabaseField(id = true)
    private String name;
    
    @DatabaseField(canBeNull = false)
    private String password;
    ...
    Account() {
    	// all persisted classes must define a no-arg constructor with at least package visibility
    }
    ...    
}


```
Creating Dao

```
 DBHelper.getInstance(context).getDao(<>.class);
```

Basic CRUD operations
```
 try {
 
 //insert
 dao.create(data);
 
 //delete
 dao.deleteById(1);
 
 //update
 dao.update(data);
 
 //query
 UserBean user = dao.queryForId(id);
    
 //List
 usrDao.queryForAll();

} catch (SQLException e) {
    e.printStackTrace();
}

```
Creating database using OrmLiteAbilitySlice
```
public class RdbSlice extends OrmLiteAbilitySlice<DBHelper>  {


    @Override
    public Class getRdbOpenHelperClass() {
        return DBHelper.class;
    }
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        view = (ComponentContainer) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_first_layout, null, false);
        setUIContent(view);
    }
    
      @Override
    public void onActive() {
        super.onActive();
        try {
            usrDao = getHelper().getDao(UserBean.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
```
# Supported Features
------------------
* Supports Dao by providing ohos components such as OrmLiteAbilitySlice , OrmLiteRdbOpenHelper

 1) DataBase Connection Source
 2) Dao Creation and CRUD operations 
 3) Batch operation

# Installation tutorial
------
ormlite is dependent on ormlite-core.

1.For using Ormlite module in sample app, include the dependent library (ormlite-core - Core ORMLite functionality that provides a lite Java ORM in conjunction with ormlite-jdbc or ormlite-ohos), add below dependencies and include the "ormlite-core.har" in libs folder of "entry" module to generate hap/har or add ormlite-core maven in implementation as below:

    Modify entry build.gradle as below :
    ```
    dependencies {
		implementation fileTree(dir: 'libs', include: ['*.har'])
    	        implementation group: 'com.j256.ormlite', name: 'ormlite-core', version: '5.3'
    	        implementation project(path: ':ormlite')
    }
    ```


2. For using ormlite in separate application make sure to add the ormlite.har in entry libs folder and add dependent library (ormlite-core - Core ORMLite functionality that provides a lite Java ORM in conjunction with ormlite-jdbc or ormlite-ohos) files as mentioned below

    Modify entry build.gradle as below :
    ```
    dependencies {
		implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
		implementation group: 'com.j256.ormlite', name: 'ormlite-core', version: '5.3'
    }
    ```

3. For using ormlite from a remote repository in separate application, add the below dependencies and include "ormlite-core.har" in libs folder of "entry" module  :

    	Modify entry build.gradle as below :
    	```
    	dependencies {
    	    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
			implementation group: 'com.j256.ormlite', name: 'ormlite-core', version: '5.3'
    	    implementation 'io.openharmony.tpc.thirdlib:ormlite-ohos:1.0.1'
    	}
