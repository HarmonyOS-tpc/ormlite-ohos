/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.j256.ormlite.utils;

/**
 * Function description
 * class Constants
 *
 */
public class Constants {
    /**
     * DATABASENAME
     */
    public static final String DATABASENAME = "SQLite";

    /**
     * MAGIC_20 ID
     */
    public static final int MAGIC_20 = 20;

    /**
     * MAGIC_4096 ID
     */
    public static final int MAGIC_4096 = 4096;
    /**
     * MAGIC_READ ID
     */
    public static final String MAGIC_READ = "readOnly";

    /**
     * MAGIC_COLUM_NAME ID
     */
    public static final String MAGIC_COLUM_NAME = "foreignColumnName";

    /**
     * MAGIC_VERSION ID
     */
    public static final String MAGIC_VERSION = "version";

    /**
     * MAGIC_AUTO ID
     */
    public static final String MAGIC_AUTO = "foreignAutoCreate";

    /**
     * MAGIC_FULLCOL ID
     */
    public static final String MAGIC_FULLCOL = "fullColumnDefinition";

    /**
     * MAGIC_FULLCOL_DEF ID
     */
    public static final String MAGIC_FULLCOL_DEF = "columnDefinition";

    /**
     * MAGIC_FULLCOL_ALLOW ID
     */
    public static final String MAGIC_FULLCOL_ALLOW = "allowGeneratedIdInsert";

    /**
     * PERSISTANT ID
     */
    public static final String PERSISTANT = "persisterClass";

    /**
     * MAX_FOREIGN_AUTO ID
     */
    public static final String MAX_FOREIGN_AUTO = "maxForeignAutoRefreshLevel";

    /**
     * MAX_FOREIGN_AUTO_REFRESH ID
     */
    public static final String MAX_FOREIGN_AUTO_REFRESH = "foreignAutoRefresh";

    /**
     * UNIQUE_INDEX ID
     */
    public static final String UNIQUE_INDEX = "uniqueIndexName";

    /**
     * INDEX_NAME ID
     */
    public static final String INDEX_NAME = "indexName";

    /**
     * UNIQUE_INDEX_DETAILS ID
     */
    public static final String UNIQUE_INDEX_DETAILS = "uniqueIndex";

    /**
     * INDEX ID
     */
    public static final String INDEX = "index";

    /**
     * UNIQUE_COMBO ID
     */
    public static final String UNIQUE_COMBO = "uniqueCombo";

    /**
     * UNIQUE ID
     */
    public static final String UNIQUE = "unique";

    /**
     * FORMAT ID
     */
    public static final String FORMAT = "format";

    /**
     * PERSISTED ID
     */
    public static final String PERSISTED = "persisted";

    /**
     * THROW_IF_NULL ID
     */
    public static final String THROW_IF_NULL = "throwIfNull";

    /**
     * UNKWNOWN_ENUM ID
     */
    public static final String UNKWNOWN_ENUM = "unknownEnumName";

    /**
     * USEGET_SET ID
     */
    public static final String USEGET_SET = "useGetSet";

    /**
     * FORIEGN ID
     */
    public static final String FORIEGN = "foreign";

    /**
     * GENERATED_SEQ ID
     */
    public static final String GENERATED_SEQ = "generatedIdSequence";

    /**
     * GENRATED_ID ID
     */
    public static final String GENRATED_ID = "generatedId";

    /**
     * ID ID
     */
    public static final String ID = "id";

    /**
     * CAN_BE_NULL ID
     */
    public static final String CAN_BE_NULL = "canBeNull";

    /**
     * WIDTH ID
     */
    public static final String WIDTH = "width";

    /**
     * DEFAULT_VAL ID
     */
    public static final String DEFAULT_VAL = "defaultValue";

    /**
     * DATA_TYPE ID
     */
    public static final String DATA_TYPE = "dataType";

    /**
     * COL_NAME ID
     */
    public static final String COL_NAME = "columnName";

    /**
     * MAGIC_MINUS_ONE ID
     */
    public static final int MAGIC_MINUS_ONE = -1;

    /**
     * MAGIC_4 ID
     */
    public static final int MAGIC_4 = 4;

    /**
     * SS ID
     */
    public static final String SS = "-s";


    private Constants() {
        /* Do nothing */
    }
}
