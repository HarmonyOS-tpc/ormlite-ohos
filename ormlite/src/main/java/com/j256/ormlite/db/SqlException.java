/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.j256.ormlite.db;

/**
 * SqlException
 *
 */
public class SqlException extends RuntimeException {
    /**
     *
     * default constructor
     *
     */
    public SqlException() { }

    /**
     *
     * exception.
     *
     * @param error sql
     *
     */
    public SqlException(String error) {
        super(error);
    }

    /**
     *
     * create sql exception.
     *
     * @param error rfjrkl
     * @param cause fjrnffn
     */
    public SqlException(String error, Throwable cause) {
        super(error, cause);
    }
}